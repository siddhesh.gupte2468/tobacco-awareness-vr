﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ShowScores : MonoBehaviour {

    public TextMeshProUGUI adsChanged;
    public TextMeshProUGUI adsSeen;
    private CountADsSeen countAdsScript;

	// Use this for initialization
	void Start () {
        countAdsScript = GameObject.Find("FPSController").GetComponent<CountADsSeen>();
	}
	
	// Update is called once per frame
	void Update () {

        adsChanged.text = "Tobacco ADs changed: " + countAdsScript.getChangedAds().ToString();

        adsSeen.text = "Tobacco ADs exposed to: " + countAdsScript.getSeenAds().ToString();

	}
}
