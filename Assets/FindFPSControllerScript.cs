﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindFPSControllerScript : MonoBehaviour {

    GameObject fpsController;
    CountADsSeen countAdsScript;

    // Use this for initialization
    void Start () {
		
        fpsController = GameObject.Find("FPSController");
        countAdsScript = fpsController.GetComponent<CountADsSeen>();
    }
	
	public void SawAd()
    {
        countAdsScript.SeeAd();
    }

    public void changedAd()
    {
        countAdsScript.ChangeAd();
    }
}
