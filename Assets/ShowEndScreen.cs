﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShowEndScreen : MonoBehaviour {

    public GameObject endScreen;
    private Waypoint waypointScript;

	// Use this for initialization
	void Start () {

        waypointScript = transform.GetComponent<Waypoint>();
    }
	
	// Update is called once per frame
	void Update () {

        if (waypointScript.occupied)
        {
            endScreen.SetActive(true);
        }

	}
}
