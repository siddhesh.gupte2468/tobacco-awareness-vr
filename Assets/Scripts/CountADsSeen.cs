﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CountADsSeen : MonoBehaviour {

    private int adsSeen = 0;
    private int adsChanged = 0;

    public void SeeAd()
    {
        adsSeen += 1;
        Debug.Log(adsSeen);
    }

    public void ChangeAd()
    {
        adsChanged += 1;
        Debug.Log(adsChanged);
    }

    public int getSeenAds()
    {
        return adsSeen;
    }

    public int getChangedAds()
    {
        return adsChanged;
    }
}
